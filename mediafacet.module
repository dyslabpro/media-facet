<?php
/**
 * Implements hook_entity_info().
 */
function mediafacet_entity_info() {
  $return['mediafacet'] = array(
    'label' => t('Media Facet'),
    'entity class' => 'MediaFacet',
    'controller class' => 'EntityAPIControllerExportable',
    'base table' => 'mediafacet',
    'fieldable' => FALSE,
    'exportable' => TRUE,
    'entity keys' => array(
      'id' => 'mfid',
      'name' => 'name',
      'label' => 'human_name',
    ),
    'module' => 'mediafacet',
    // Enable the entity API's admin UI.
    'admin ui' => array(
      'path' => 'admin/config/search/mediafacet',
      'file' => 'mediafacet.admin.inc',
      'controller class' => 'EntityDefaultUIController',
    ),

// Uncomment for enable fieldable option.
    /*
    'bundles' => array(
      'mediafacet' => array(
        'label' => t('MediaFacet'),
        'admin' => array(
          'path' => 'admin/config/search/mediafacet',
          'access arguments' => array('administer site configuration'),
        ),
      ),
    ),
    */

    'access callback' => 'mediafacet_access',
    'deletion callback' => 'mediafacet_delete',
  );

  return $return;
}

/**
 * Implements hook_library().
 */
function mediafacet_library() {
  $path = libraries_get_path('mediaelement');
  $libraries = array();

  $libraries['mediafacet_mediaelement'] = array(
    'title' => 'Media Element',
    'website' => 'http://mediaelementjs.com/',
    'version' => '2.1.6',
    'js' => array(
      $path . '/build/mediaelement-and-player.min.js' => array('group' => JS_LIBRARY, 'preprocess' => FALSE),
      drupal_get_path('module', 'mediafacet') . '/mediafacet.js' => array('group' => JS_LIBRARY, 'preprocess' => FALSE),
    ),
    'css' => array(
      $path . '/build/mediaelementplayer.min.css' => array('group' => CSS_SYSTEM),
      drupal_get_path('module', 'mediafacet') . '/mediafacet.css' => array('group' => CSS_SYSTEM),
    ),
  );
  return $libraries;
}

/**
 * Access callback for facet context blocks.
 */
function mediafacet_access() {
  return user_access('administer mediafacet');
}

/**
 * Load mediafacet by name.
 *
 * Needed for validation of machine readable name form field.
 */
function mediafacet_load_name($mediafacet_name) {
  $entities = entity_load('mediafacet', FALSE, array('name' => $mediafacet_name));
  return reset($entities);
}

/**
 * Load function
 */
function mediafacet_delete($entity_name) {
  if ($mediafacet = mediafacet_load_name($entity_name)) {
    if ($mediafacet->fid && $file = file_load($mediafacet->fid)) {
      file_delete($file, TRUE);
    }
    return entity_get_controller('mediafacet')->delete(array($mediafacet->mfid));
  }
  return FALSE;
}

/**
 * Load function
 */
function mediafacet_load($entity_id){
  return entity_load_single('mediafacet', $entity_id);
}

/**
 * FacetContextBlock Entity class.
 */
class MediaFacet extends Entity {
  public $type;
  public $label;
  public $weight = 0;

  public function __construct($values = array()) {
    parent::__construct($values, 'mediafacet');
  }

  function isLocked() {
    return isset($this->status) && empty($this->is_new) && (($this->status & ENTITY_IN_CODE) || ($this->status & ENTITY_FIXED));
  }
}

/**
 * Implement hook_permission().
 */
function mediafacet_permission() {
  return array(
    'administer mediafacet' => array(
      'title' => t('Administer mediafacet'),
      'description' => t('Create/Edit/Delete Media Facet'),
    ),
  );
}

/**
 * Implements hook_facetapi_widgets().
 */
function mediafacet_facetapi_widgets() {
  return array(
    'mediafacet_links' => array(
      'handler' => array(
        'label' => t('MediaFacet Links'),
        'class' => 'MediaFacetWidgetLinks',
        'query types' => array('term', 'date'),
      ),
    ),
    'mediafacet_checkbox_links' => array(
      'handler' => array(
        'label' => t('MediaFacet Links with checkboxes'),
        'class' => 'MediaFacetWidgetCheckboxLinks',
        'query types' => array('term', 'date'),
      ),
    ),

    'mediafacet_images' => array(
      'handler' => array(
        'label' => t('MediaFacet Images'),
        'class' => 'MediaFacetWidgetImages',
        'query types' => array('term', 'date'),
      ),
    ),
    'mediafacet_checkbox_images' => array(
      'handler' => array(
        'label' => t('MediaFacet Images with checkboxes'),
        'class' => 'MediaFacetWidgetCheckboxImages',
        'query types' => array('term', 'date'),
      ),
    ),
  );
}

/**
 * Implements hook_menu().
 */
function mediafacet_menu() {
  return array(
    'mediafacet/%mediafacet/%' => array(
      'page callback' => 'mediafacet_view',
      'page arguments' => array(1, 2),
      'access arguments' => array('access content'),
      'type' => MENU_CALLBACK,
    )
  );
}

/**
 * Display the Facet help object
 */
function mediafacet_view($mediafacet, $action = '') {
  $markup = '<div class="mediafacet-help">'. check_markup($mediafacet->text, $mediafacet->text_format) . '</div>';
  if ($action != 'ajax') {
    return $markup;
  }
  print $markup;
}