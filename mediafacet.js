(function($, Drupal, undefined){
  Drupal.behaviors.mediafacet = {
    attach: function(context, settings) {
      $('.mediafacet-audio', context).once('mediaelement', function() {
        $(this).mediaelementplayer({
          'audioWidth': 16,
          'audioHeight': 16,
          'features': ['playpause']
        });
      })
    }
  };
})(jQuery, Drupal);