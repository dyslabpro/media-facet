<?php

/**
 * @file
 * Administrative page callbacks for the MediaFacet module.
 */

/**
 * Generates the MediaFacet editing form.
 */
function mediafacet_form($form, &$form_state, $mediafacet, $op = 'edit') {

  if ($op == 'clone') {
    $mediafacet->label .= ' (cloned)';
  }

  $type = isset($mediafacet->type) ? $mediafacet->type : 'text';

  $form['human_name'] = array(
    '#title' => t('MediaFacet name'),
    '#type' => 'textfield',
    '#required' => TRUE,
    '#default_value' => isset($mediafacet->human_name) ? $mediafacet->human_name : '',
    '#description' => t('Human name of the MediaFacet'),
    '#size' => 32,
  );

  // Machine-readable name.
  $form['name'] = array(
    '#type' => 'machine_name',
    '#required' => TRUE,
    '#default_value' => isset($mediafacet->name) ? $mediafacet->name : '',
    '#maxlength' => 128,
    '#disabled' => $mediafacet->isLocked() && $op != 'clone',
    '#machine_name' => array(
      'exists' => 'mediafacet_load_name',
      'source' => array('human_name'),
    ),
    '#description' => t('A unique machine-readable name for this MediFacet. It must only contain lowercase letters, numbers, and underscores.'),
  );

  $form['facet'] = array(
    '#title' => t('Facet value'),
    '#type' => 'textfield',
    '#required' => TRUE,
    '#default_value' => isset($mediafacet->facet) ? $mediafacet->facet : '',
    '#description' => t('Facet value pair. Example "type:article"'),
  );

  $form['type'] = array(
    '#title' => t('MediaFacet Type'),
    '#type' => 'select',
    '#required' => TRUE,
    '#options' => array(
      'text' => t('Text'),
      'audio' => t('Audio'),
      'image' => t('Image'),
    ),
    '#default_value' => $type,
  );

  $form['audio_fid'] = array(
    '#title' => t('Media File'),
    '#type' => 'managed_file',
    '#default_value' => (isset($mediafacet->fid) && $type == 'audio') ? $mediafacet->fid : '',
    '#upload_location' => file_default_scheme() . '://mediafacet_files',
    '#upload_validators' => array('file_validate_extensions' => array('mp3 wav')),
    '#description'=> t('Allowed file types: !extensions.', array('!extensions' => '<strong>mp3 wav</strong>')),
    '#states' => array(
      'visible' => array(
       ':input[name="type"]' => array('value' => 'audio'),
      ),
    ),
  );

  $form['image_fid'] = array(
    '#title' => t('Image File'),
    '#type' => 'managed_file',
    '#default_value' => (isset($mediafacet->fid) && $type == 'image') ? $mediafacet->fid : '',
    '#upload_location' => file_default_scheme() . '://mediafacet_files',
    '#upload_validators' => array('file_validate_extensions' => array('png gif jpg jpeg')),
    '#description'=> t('Allowed file types: !extensions.', array('!extensions' => '<strong>png gif jpg jpeg</strong>')),
    '#states' => array(
      'visible' => array(
       ':input[name="type"]' => array('value' => 'image'),
      ),
    ),
  );

  $form['text_wrapper'] = array(
    '#type' => 'container',
    '#states' => array(
      'visible' => array(
       ':input[name="type"]' => array('value' => 'text'),
      ),
    ),
  );

  $form['text_wrapper']['text'] = array(
    '#title' => t('Media text (shown using colorbox).'),
    '#type' => 'text_format',
    '#default_value' => isset($mediafacet->text) ? $mediafacet->text : '',
    '#format' => isset($mediafacet->text_format) ? $mediafacet->text_format : filter_default_format(),
  );


  $form['actions'] = array('#type' => 'actions');
  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save MediaFacet'),
    '#weight' => 40,
  );

  if (!$mediafacet->isLocked() && $op != 'add' && $op != 'clone') {
    $form['actions']['delete'] = array(
      '#type' => 'submit',
      '#value' => t('Delete MediaFacet'),
      '#weight' => 45,
      '#limit_validation_errors' => array(),
      '#submit' => array('mediafacet_form_submit_delete')
    );
  }

  return $form;
}

/**
 * Validation of the MediaFacet form submission.
 */
function mediafacet_form_validate($form, &$form_state) {
  $facet = $form_state['values']['facet'];
  $type = $form_state['values']['type'];
  $matches = array();
  if (!preg_match('/^([a-zA-Z0-9_-]*:[a-zA-Z0-9_-]*|[a-zA-Z0-9_-]*:[a-zA-Z0-9_-]*:[a-zA-Z0-9_-]*)(,[a-zA-Z0-9_-]*:[a-zA-Z0-9_-]*|,[a-zA-Z0-9_-]*:[a-zA-Z0-9_-]*:[a-zA-Z0-9_-]*)*$/', $facet, $matches) && !empty($facet)) {
    form_set_error('facets', t('Incorrect facet value. Example: field_tags:5'));
  }

  if ($type != 'text' && (!isset($form_state['values'][$type . '_fid']) || empty($form_state['values'][$type . '_fid']))) {
    form_set_error($type . '_fid', t('File field is required'));
  }
}

/**
 * Submit handler for creating/editing MediaFacet.
 */
function mediafacet_form_submit($form, &$form_state) {
  // Prepare text value.
  if (isset($form_state['values']['text'])) {
    $text = $form_state['values']['text'];
    $form_state['values']['text'] = isset($text['value']) ? $text['value'] : '';
    $form_state['values']['text_format'] = isset($text['format']) ? $text['format'] : filter_default_format();
  }

  $type = $form_state['values']['type'];
  if ($type != 'text' && isset($form_state['values'][$type . '_fid'])) {
    $form_state['values']['fid'] = $form_state['values'][$type . '_fid'];
  }


  unset($form_state['values']['audio_fid']);
  unset($form_state['values']['image_fid']);

  if (isset($form_state['mediafacet']->fid)) {
    $old_fid = $form_state['mediafacet']->fid;
  }

  $mediafacet = entity_ui_form_submit_build_entity($form, $form_state);

  // Save and go back.
  entity_save('mediafacet', $mediafacet);

  // Set the permanent status for attached files.
  if ($mediafacet->fid && $file = file_load($mediafacet->fid)) {
    // Delete old file if exists.
    if (isset($old_fid) && $old_fid != $mediafacet->fid && $oldfile = file_load($old_fid)) {
      file_delete($oldfile, TRUE);
    }
    if (!$file->status) {
      $file->status = FILE_STATUS_PERMANENT;
      file_save($file);
      file_usage_add($file, 'mediafacet', $mediafacet->type, $mediafacet->mfid);
    }
  }

  // Redirect user back to list of task types.
  $form_state['redirect'] = 'admin/config/search/mediafacet';
}

function mediafacet_form_submit_delete(&$form, &$form_state) {
  $form_state['redirect'] = 'admin/config/search/mediafacet/manage/' . $form_state['mediafacet']->name . '/delete';
}


